st=set()
st.add(1)              # allows addition but no replacement
st.add(2)
st.add(3)
st.add(4)
st.add(5)
# print(st)

fs=frozenset([1,2,3,3,3,4])
# fs.add(2)               #error: 'Frozenset object has no attribute ADD'; it is immutable
print(fs)
for i in fs:
    print(i)