# Starts with curly braces; bu no key:value pairs
a={1,2,3,4,5,5,6,6,6,5,'python','django'}                 # Don't print duplicate values
l=[1,2,3,4,5,5,6,6,6,5,'python','django']
# print(type(a))
print(a)
# print(a[0])                   Can't index them; bcz it prints values in any order             
print(l)          

ls=[1,20,300,20,300,1]
convert=set(ls)                 # can convert another data type to set
print(convert)

# Get all elements
for i in convert:
    print(i)