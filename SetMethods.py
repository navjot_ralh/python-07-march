s1=set()
# Add elements in set
s1.add(1)
s1.add(1)
s1.add(2)
s1.add(3)
s1.add(4)

s2={2,3,4,5,6}
s3={5,6,7,8,9}
# evns=set()
# for i in range(1,100,2):
#     evns.add(i)
# print(evns)

# Union of sets
# u=s1.union(s2)
# u=s2.union(s3)
# u=s2|s3
# print('Union:',u)

# Intersection
# ints=s2.intersection(s3)
# ints=s2&s3
# print('intersection:',ints)

# Difference
# d=s2.difference(s3)
# d=s1-s2
# print('Difference:',d)

# Membership
# print(100 in s2)
# print(2 in s2)
print(100 not in s2)
print(2 not in s2)